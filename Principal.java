public class Principal {
    public static void main(String[] args) {
        System.out.println("Hola, bienvenido al paradigma Orientado a Objetos");
        Persona juan= new Persona();
        String nombre = juan.decirNombre();
        int edad = juan.decirEdad();
        Saludo gonzalo = new Saludo();
        String saludo = gonzalo.devolverSaludo();
        System.out.println("Hola como estas? " + nombre);
        System.out.println("La Edad es: "+ edad);
        System.out.println(saludo);
    }
    
}